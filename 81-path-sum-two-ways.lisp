(load "p081_matrix.txt")

(defparameter ar
  (make-array '(80 80) :initial-contents input :adjustable nil))

(defparameter matrix
  (make-array '(5 5)
			  :initial-contents
			  '((131 673 234 103 18)
				(201 96 342 965 150)
				(630 803 746 422 111)
				(537 699 497 121 956)
				(805 732 524 37 331))))

(defun add-vertex (a b graph cost-matrix)
  (unless (nth-value 1 (gethash a graph))
	(setf (gethash a graph) nil))
  (push (cons b (aref cost-matrix (car b) (cadr b)))
		(gethash a graph)))

(defun make-graph (matrix)
  (let ((graph (make-hash-table :test #'equal)))
	(loop for x from 0 below (array-dimension matrix 0) do
	  (loop for y from 0 below (array-dimension matrix 1) do
		  (cond ((= x y 79) (add-vertex `(,x ,y) (list x y) graph matrix))
				((= x 79) (add-vertex `(,x ,y) (list x (1+ y)) graph matrix))
				((= y 79) (add-vertex `(,x ,y) (list (1+ x) y) graph matrix))
				(t (progn
					 (add-vertex `(,x ,y) (list (1+ x) y) graph matrix))
		  		   (add-vertex `(,x ,y) (list x (1+ y)) graph matrix)))))
	graph))

(defun pprint-graph (graph)
  (loop for k being the hash-keys of graph using (hash-value v) do
		(format t "~a -> ~a~%" k v)))

(defparameter g (make-graph ar))

(defun dijkstra (graph source)
  (let ((distance (make-hash-table :test #'equal))
		(q '()))
	(setf (gethash source distance) 0)
	(loop for vertex being the hash-keys of graph using (hash-value neighbors)
		  do
			 (unless (equal vertex source)
			   (setf (gethash vertex distance) most-positive-fixnum))
			 (push vertex q))
	(loop while (not (endp q))
		  for v = (loop for v in q
						for min = most-positive-fixnum
						for min-vertex = nil
						when (< (gethash v distance) min)
						  do
							 (setf min (gethash v distance))
							 (setf min-vertex v)
						end
						finally (return min-vertex))
		  do
			 (setf q (remove v q :test #'equal))
			 (loop for u in (gethash v graph)
				   when (member (car u) q :test #'equal)
					 do
						(let ((alt (+ (gethash v distance)
									  (cdr u))))
						  (when (< alt (gethash (car u) distance))
							(setf (gethash (car u) distance) alt)))))
	distance))
