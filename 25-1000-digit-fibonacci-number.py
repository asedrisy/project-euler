def fib_iter():
    a = 1
    b = 1
    n = 2
    while True:
        f = a + b
        a, b = b, f
        n += 1
        if f > 10**999:
            return n

print fib_iter()
