from math import sqrt

abundants = []
sum_abundants = {}
def divisors(n):
    s = []
    for x in xrange(1, ((n/2) +1)):
        if n % x == 0:
            s.append(x)
    return s

# Get all abundant numbers
for n in xrange(12, 28123 + 1):
    if sum(divisors(n)) > n:
        abundants.append(n)

# Get all possible combination sums
for x in abundants:
    for y in abundants:
        sum_abundants[x + y] = True

s = 0
for n in xrange(1, 28123 + 1):
    # FOUT!!!
    # if all(x not in sum_abundants for x in d):
    if n not in sum_abundants:
        s += n
print s

#4179871
#40570209
#219538601
#120120587
