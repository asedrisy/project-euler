(defparameter ht nil)
(defparameter s nil)
(defparameter memo (make-hash-table :test #'equal :size 2603))
					
(defun dimensions (x y xp yp)
  (unless (or (or (> x xp) (> y yp))
			  (gethash (list x y) ht))
	(setf (gethash (list x y) ht) t)
	(incf s (* x y))
	(progn (dimensions (1+ x) y xp yp)
		   (dimensions x (1+ y) xp yp))))

(defun run-test (x y)
  (setf ht (make-hash-table :test #'equal :size 5000))
  (setf s 0)
  (let ((m (gethash (list x y) memo)))
	(when (null m)
	  (dimensions 1 1 x y)
	  (setf (gethash (list x y) memo) s)
	  (setf m s))
	m))

(defparameter dim
  (loop for x from 1 upto 100
		nconc
		(loop for y from 1 upto 100
			  for res = (run-test x y)
			  collect (list x y res (abs (- 2000000 res))))))

(let ((dim (loop for x from 30 upto 80
				 nconc
				 (loop for y from 30 upto 80
					   for res = (run-test x y)
					   collect (list x y res (abs (- 2000000 res)))))))
  (print (first (sort (copy-list dim) #'< :key #'fourth))))
