#lang racket

;; Get all the factors of N.
(define (factors n)
  (define (*factors d end)
    (cond ((> d end) '())
          ;; factors come in pairs, 
          ;; the smaller of which is no bigger 
          ;; than (sqrt n)
          ((= (modulo n d) 0) (cons d (cons (quotient n d) 
                                            (*factors (+ d 1) end))))
          (else (*factors (+ d 1) end))))
  (*factors 1 (sqrt n)))

;; Get the N-th triangle number.
;; triangle(N) = sum 1 .. N.
(define triangle
  (λ (n)
    (define *triangle
      (λ (l)
        (cond
          ((> l n) 0)
          (else (+ l (*triangle (+ l 1)))))))
  (*triangle 1)))

;; Get the triangle number with a number of divisors more than N.
(define divisors-over 
  (λ (divisors)
    (define iter
      (λ (current-number)
        (cond
          ((> (length (factors (triangle current-number))) divisors) (triangle current-number))
          (else (iter (+ 1 current-number))))))
  (iter 1)))

(divisors-over 500)
    
    
    