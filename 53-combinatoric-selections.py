def iterate(layers):
    triangle = []
    triangle.append([1])
    triangle.append([1, 1])
    #triangle.append([1, 2, 1])
    for i in range(2, layers + 1):
        v = [1]
        for j in range(1, i):
            v.append(triangle[i-1][j-1] + triangle[i-1][j])
        v.append(1)
        triangle.append(v)
    return triangle
        
triangle = iterate(100)
#for layer in triangle:
#    print layer

n= 0
for layer in triangle:
    for e in layer:
        if e > 1000000:
            n += 1
            #print layer, e

print n
