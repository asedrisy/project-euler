from itertools import permutations

#def join_digits(digits_list, l):

def _try_composite(a, d, n, s):
    if pow(a, d, n) == 1:
        return False
    for i in range(s):
        if pow(a, 2**i * d, n) == n-1:
            return False
    return True # n  is definitely composite
 
def is_prime(n, _precision_for_huge_n=16):
    if n in _known_primes or n in (0, 1):
        return True
    if any((n % p) == 0 for p in _known_primes):
        return False
    d, s = n - 1, 0
    while not d % 2:
        d, s = d >> 1, s + 1
    # Returns exact according to http://primes.utm.edu/prove/prove2_3.html
    if n < 1373653: 
        return not any(_try_composite(a, d, n, s) for a in (2, 3))
    if n < 25326001: 
        return not any(_try_composite(a, d, n, s) for a in (2, 3, 5))
    if n < 118670087467: 
        if n == 3215031751: 
            return False
        return not any(_try_composite(a, d, n, s) for a in (2, 3, 5, 7))
    if n < 2152302898747: 
        return not any(_try_composite(a, d, n, s) for a in (2, 3, 5, 7, 11))
    if n < 3474749660383: 
        return not any(_try_composite(a, d, n, s) for a in (2, 3, 5, 7, 11, 13))
    if n < 341550071728321: 
        return not any(_try_composite(a, d, n, s) for a in (2, 3, 5, 7, 11, 13, 17))
    # otherwise
    return not any(_try_composite(a, d, n, s) 
                   for a in _known_primes[:_precision_for_huge_n])
_known_primes = [2, 3]
_known_primes += [x for x in range(5, 1000, 2) if is_prime(x)]

def is_pandigital(n):
    correct_sums = [1, 3, 6, 10, 15, 21, 28, 36, 45]
    correct_products = [1, 2, 6, 24, 120, 720, 5040, 40320, 362880]
    s = 0
    k = 0
    p = 1
    while n > 0:
        digit = n % 10
        k += 1
        s += digit
        p *= digit
        n /= 10
    return correct_sums[k-1] == s and correct_products[k-1] == p

def join_digits(lst, n):
    sum = 0
    k = 1
    n -= 1
    while n >= 0:
        sum += (lst[n] % 10) * k
        k *= 10
        n -= 1
    return sum

values = []
for p in permutations([1, 2, 3, 4, 5, 6, 7]):
    v = join_digits(p, 7)
    if is_prime(v):
        values.append(v)

print max(values)
