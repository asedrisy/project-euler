def digit_sum(n):
    s = 0
    while n > 0:
        s += n % 10
        n /= 10
    return s

sums = []
for a in range(1, 100):
    for b in range(1, 100):
        sums.append(digit_sum(a ** b))
print max(sums)