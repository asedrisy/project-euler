from fractions import Fraction

values = []

for p in range(10, 99 + 1):
    for q in range(p, 99 + 1):
        v = (p * 1.0) / (q * 1.0)
        a = str(p)
        b = str(q)
        f = False
        if (((p % 10) != 0) and (((q % 10) != 0)) and (p != q)):
            if a[0] == b[0]:
                ta = int(a[1])
                tb = int(b[1])
                f = True
            elif a[0] == b[1]:
                ta = int(a[1])
                tb = int(b[0])
                f = True
            elif a[1] == b[0]:
                ta = int(a[0])
                tb = int(b[1])
                f = True
            elif a[1] == b[1]:
                ta = int(a[0])
                tb = int(b[0])
                f = True

            if f:
                t = (ta * 1.0) / (tb * 1.0)
                if (t == v) and (a[0] != a[1]) and (b[0] != b[1]):
                    values.append((p, q))

[(16, 64), (19, 95), (26, 65), (49, 98)]
num = 1
for v in values:
	num *= v[0]
den = 1
for v in values:
	den *= v[1]

print Fraction(num, den)
