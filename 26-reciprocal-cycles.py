def long_division(n, d):
    result = []
    n_list = [0]
    while True:
        if d > n:
            n = n * 10

        if n in n_list:
            return result
        
        result.append(n / d)
        n_list.append(n)

        n = n % d

        
m = 0
current_max = []
n = 0

for x in xrange(1, 1000):
    r = long_division(1, x)
    if len(r) > m:
        current_max = r
        m = len(r)
        n  = x

print n
