powers = []
for x in xrange(0, 10):
    powers.append(x ** 5)

def sum_digits_power(n):
    s = 0
    while n > 0:
        s += powers[n % 10]
        n = n / 10
    return s

l = []
for x in xrange(2, 355000):
    if x == sum_digits_power(x):
        l.append(x)

print sum(l)
