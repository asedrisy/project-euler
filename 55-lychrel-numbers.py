def reverse(n):
    copy_n = n
    rev = 0
    while n > 0:
        rev = rev * 10 + (n % 10)
        n /= 10
    return rev

def is_palindrome(n):
    copy_n = n
    rev = 0
    while n > 0:
        rev = rev * 10 + (n % 10)
        n /= 10
    return rev == copy_n
    

def is_lychrel(n):
    times = 0
    while times < 50:
        sum = n + reverse(n)
        if is_palindrome(sum):
            return False
        else:
            n = sum
            times += 1
    return True

def main():
    lychrel_numbers = []
    for n in range(1, 10001):
        if is_lychrel(n):
            lychrel_numbers.append(n)
    print len(lychrel_numbers)
    

main()
