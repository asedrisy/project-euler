#lang racket

(define (factor n)
  (find-factors (sqrt n) n 2 '()))

;; Is a divisible by b?
(define (divides? a b)
  (= (remainder a b) 0))

;; L: list to which factors are append to
;; d: current divisor
;; n: number to be factored
(define (find-factors sqrt-n curr-n div factors-list)
  (cond ((> div sqrt-n) factors-list)
        ;; curr-n is divisible by div, try quotient 
        ((divides? curr-n div) (find-factors sqrt-n
                                             (/ curr-n div)
                                             div
                                             (list factors-list div)))
        (else (find-factors sqrt-n
                            curr-n
                            (+ div 1)
                            factors-list))))

(factor 600851475143)