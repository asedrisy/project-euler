from math import log

#with open("p099_base_exp.txt") as f:
#    content = f.readlines()
#lines = [line.rstrip('\n') for line in file]
FILE = "p099_base_exp.txt"
lines = [line.rstrip('\n') for line in open(FILE)]
lines = [l.split(',') for l in lines]
numbers = []
for l in lines:
    n = []
    for m in l:
        n.append(int(m))
    numbers.append(n)

i = 1

base_exponents = []

for n in numbers:
    b = [i]
    for m in n:
        b.append(m)
    base_exponents.append(b)
    i += 1

results = []

for n in base_exponents:
    r = log(n[1])
    results.append((n[0], r * n[2]))

print sorted(results, key = lambda x: x[1], reverse=True)[0]
