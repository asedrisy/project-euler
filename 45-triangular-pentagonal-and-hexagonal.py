def triangle(n):
	return (n * (n+1))/2
def pentagonal(n):
	return (n*(3*n - 1))/2
def hexagonal(n):
	return (n*(2*n - 1))

values = {}

for i in range(284, 100000):
	values[triangle(i)] = [i]

for i in range(164, 100000):
    v = pentagonal(i)
    if v in values:
        values[v].append(i)

for i in range(142, 100000):
    v = hexagonal(i)
    if v in values:
        values[v].append(i)
        
for k in values:
    if len(values[k]) == 3:
        print k
