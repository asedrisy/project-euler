data = []

def read_file():
    with open("f.txt") as f:
        content = f.readlines()
        content = [x.strip('\n') for x in content] 

    for s in content:
        data.append(s.split(' '))

    for i in range(len(data)):
        data[i] = map(int, data[i])


read_file()
LAYERS = len(data) - 1

cl = LAYERS
for layer in range(LAYERS-1, -1, -1):
    for index in range(0, cl):
        data[layer][index] += max(data[layer+1][index], data[layer+1][index+1])
    cl -= 1

print data[0][0]
