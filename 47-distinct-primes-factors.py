import math

def memoize(f):
    memo = {}
    def helper(x):
        if x not in memo:
            memo[x] = f(x)
        return memo[x]
    return helper


@memoize
def isprime (n):
    if n == 1:
        return False
    elif n == 2:
        return True
    else:
        for x in range (2, int(math.sqrt(n))+1):
            if n % x == 0:
                return False
                break
        else:
            return True

def factors (a, prev):
    if a == 1:
        return 1
    elif isprime(a):
        return 1
    else:
        for x in range (2, int(math.sqrt(a))+1):
            if a % x == 0:
                q = a/x
                if prev == x:
                    return 0 + factors(q, x)
                else:
                    
                    return 1 + factors(q, x)
count = 0

##for i in range(650, 1000000):
##    n = factors(i,0)
##    if n == 4:
##        count += 1
##    if n != 4:
##        count = 0
##    if count == 4:
##        print i - 4
##        break
for i in range(134035, 134050):
    print i, factors(i, 0)
