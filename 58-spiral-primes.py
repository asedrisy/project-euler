from sympy import isprime

p = 1
step = 2
diagonals = p
primes = 0
side_length = 1


while True:
        side_length += 2
        
        for diag in range(4):
            diagonals += 1
            p += step
            if isprime(p):
                primes += 1
            #sum += p
        step += 2
        if (primes * 1.0) / diagonals < 0.1:
            break
print side_length
#print (primes * 1.0) / diagonals
