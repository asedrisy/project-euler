def is_palindrome(n):
	if str(n) == str(n)[::-1]:
		return True
	else:
		return False

for i in range(99, 1001):
	for j in range(99, 1001):
		if is_palindrome(i * j):
			print i, j, i * j

# Of:
print max(x * y
	for x in xrange(100, 1000)
		for y in xrange(100, 1000)
			if str(x * y) == str(x * y)[::-1])
