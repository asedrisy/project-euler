numbers = range(1, 21)

for i in xrange(2520, 999999999, 2520):
    if all(i % n == 0 for n in numbers):
        print i
        
def gcd(a, b):
    if (b == 0): return a
    else: return gcd(b, a%b)

def lcm(a, b):
    return abs(a*b) / gcd(a, b)

def euler5(n):
    if (n == 1): return 1
    else: return lcm(n, euler5(n-1))

print euler5(20)