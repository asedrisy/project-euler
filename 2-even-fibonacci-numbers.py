sum = 0
n = 4000000

old = 1
current = 1


while current < n:
	if current % 2 == 0:
		sum += current
	next = current + old
	old = current
	current = next
	
print sum