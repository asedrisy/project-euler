import time
t1 = time.clock()

values = {}

def A(n, k):
    if n < 0:
        return 0
    elif n == 0 or n == 1:
        return 1
    elif k == 1:
        return 1
    else:
        if (n, k-1) in values:
            a = values[(n, k-1)]
        else:
            a = A(n, k-1)
            values[(n, k-1)] = a
        if (n-k, k) in values:
            b = values[(n-k, k)]
        else:
            b = A(n-k, k)
            values[(n-k, k)] = b
        return a + b
 
print A(100, 100) - 1


print time.clock() - t1

