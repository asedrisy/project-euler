(defun first-unreduced-denominator (numerator)
  (loop for denominator from (1+ numerator) upto 1000000
		when (/= (gcd numerator denominator) 1) do
		  (return denominator)))

(defun period (denominator)
  (let ((count-reduced-fraction 0)
		
  (let ((numerator 5))
