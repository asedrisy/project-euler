import sympy

def truncatable_prime(n):
    # right to left: / 10
    # left to right
    if not sympy.isprime(n):
        return False
    ndiv = n / 10
    nmod = n % 10
    mod = 10
    div = 10
    while ndiv > 0:
        if sympy.isprime(ndiv) and sympy.isprime(n % mod):
            mod *= 10
            div *= 10
            ndiv /= 10
        else:
            return False
    return True
            
            
N = 3797
print truncatable_prime(N)

hits = 0
n = 9
sum = 0
while hits < 11:
    if truncatable_prime(n):
        hits += 1
        print n
        sum += n
    n += 2

