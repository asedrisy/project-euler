from math import sqrt

def make_rat(n, d):
    def gcd(a, b):
        if b == 0:
            return a
        else:
            return gcd(b, a % b)
    if (n == d == 0):
        return (0, 0)
    else:
        g = gcd(n, d)
        return (n/g, d/g)
def numer(x):
    return x[0]
def denom(x):
    return x[1]
def print_rat(x):
    print x[0], "/", x[1]

def add_rat(x, y):
    return make_rat((numer(x) * denom(y)) + (numer(y) * denom(x)), (denom(x) * denom(y)))
def sub_rat(x, y):
    return make_rat((numer(x) * denom(y)) - (numer(y) - denom(x)),
                    denom(x) * denom(y))
def mult_rat(x, y):
    return make_rat(numer(x) * numer(y), denom(x) * denom(y))
def div_rat(x, y):
    return make_rat(numer(x) * denom(y), denom(x) * numer(y))
def is_equal_rat(x, y):
    return numer(x) * denom(y) == numer(y) == denom(x)
    

#one_half = make_rat(5, 10)
#print_rat(add_rat(one_half, one_half))
#print_rat(mult_rat(one_half, one_half))

ONE_RAT = make_rat(1, 1)

def sqrt_2(n):
    result = make_rat(1, 2)
    for i in range(n):
        sum = add_rat(result, make_rat(2,1))
        result = div_rat(ONE_RAT, sum)

    print_rat(add_rat(ONE_RAT, result))

#sqrt_2(2)
#sqrt_2(50)

L= []
counter = 2
j = 2
for i in range(98):
    if counter % 3 == 0:
        L.append(j)
        j += 2
    else:
        L.append(1)
    counter += 1
print L

result = make_rat(1, 1)
for i in reversed(L):
    sum = add_rat(result, make_rat(i, 1))
    result = div_rat(ONE_RAT, sum)

    
def sum_digits(n):
    s = 0
    while n > 0:
        s += n % 10
        n /= 10
    return s

result = add_rat(make_rat(2, 1), result)
print result
print sum_digits(numer(result))

