from sympy import sieve, isprime
from array import array
sieve._list = array('l', [2, 3, 5, 7, 11, 13])

END = 1000000

values = {}

def main():
    for a in sieve.primerange(1, END):
        s = a
        steps = 1
        for b in sieve.primerange(a+1, END):
            steps += 1
            s += b
            if s > END:
                break
            if isprime(s):
                if s in values:
                    k = values[s]
                    if steps > k:
                        values[s] = steps
                else:
                    values[s] = steps

main()

m = max(values, key=values.get)
print m, values[m]
