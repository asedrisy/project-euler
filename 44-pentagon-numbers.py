by_index = {}
by_value = {}

pairs_tried = {}

def is_pentagonal(n):
    return n in by_value

def pentagonal(n):
    if n in by_index:
        return by_index[n]
    else:
        s = (n*(3*n - 1)) / 2
        by_index[n] = s
        by_value[s] = True
        return s
    
minimum = []

for i in range(1, 10000):
    pi = pentagonal(i)
    for j in range(1, 10000):
        if (i, j) in pairs_tried or (j, i) in pairs_tried:
            continue
        else:
            pj = pentagonal(j)
            s = pi + pj
            d = abs(pi - pj)
            if is_pentagonal(s) and is_pentagonal(d):
                print d
                minimum.append(d)
            pairs_tried[(i, j)] = True
            pairs_tried[(j, i)] = True
print min(minimum)
