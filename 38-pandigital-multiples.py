from itertools import permutations

def reverse(n):
    copy_n = n
    rev = 0
    while n > 0:
        rev = rev * 10 + (n % 10)
        n /= 10
    return rev

def concatenate(a, b):
    b = reverse(b)
    while b > 0:
        a *= 10
        a += b % 10
        b /= 10
    return a

def first_d_digits(n, d):
    r = reverse(n)
    sum = 0
    while d > 0:
        sum = sum*10 + r % 10
        r /= 10
        d -= 1
    return sum

def is_concatenated_product(n, test):
    sum = 0
    i = 1
    while True:
        product = n * i
        sum = concatenate(sum, product)
        if sum == test:
            return True
        elif sum > test:
            return False
        i += 1
        

def main():
    possibilities = [int("".join(x)) for x in permutations("123456789")]
    possibilities.sort(reverse=True)

    for p in possibilities:
        for d in range(1, 9):
            first_d = first_d_digits(p, d)
            if is_concatenated_product(first_d, p):
                print p
                return
main()
    
#print concatenate(123, 455)
#print first_d_digits(123456789, 4)
#print is_concatenated_product(192, 192384576)
