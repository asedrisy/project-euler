from fractions import Fraction

test_value = 3.0 / 7.0
lower = Fraction(2, 5)

N = 1000000
for denom in range(8, N+1):
    found = False
    denom_decim = denom * 1.0
    left_numer = 0
    right_numer = denom
    while not found:
        #print left_numer, right_numer, denom
        difference = right_numer - left_numer
        mp_numer = (left_numer + right_numer) / 2
        mp_value = mp_numer / denom_decim
                    
        if mp_value > test_value:
            right_numer = mp_numer - 1
        elif mp_value < test_value:
            left_numer = mp_numer
        elif mp_value == test_value:
            test_this = Fraction(left_numer, denom)
            found = True
        if difference == 0:
            test_this = Fraction(left_numer, denom)
            found = True
            break
        elif difference == 1:
            right_value = right_numer / denom_decim
            left_value = left_numer / denom_decim
            if right_value > test_value:
                test_this = Fraction(left_numer, denom)
                
            elif right_value < test_value:
                test_this = Fraction(right_numer, denom)
            found = True
    if test_this > lower:
        lower = test_this
        #print lower

print Fraction(lower)
         
            
        

