(deftype smallish-integer (&optional (bits 31))
  `(signed-byte ,bits))

(declaim (ftype (function (smallish-integer smallish-integer) double-float)
                smallish-integertest)
         (inline smallish-integertest))

(defvar board nil)

(defun euler-96 ()
  (declare (type (SIMPLE-ARRAY smallish-integer (9 9)) board))
  (let ((puzzles nil)
		(sum 0))
	(declare (type smallish-integer sum))
	(with-open-file (in "p096_sudoku.txt")
	  (loop for line = (read-line in nil)
			while line do
			  (push 
			   (loop for i from 1 upto 9
					 collect (map 'list #'(lambda (c) (- (char-int c) 48)) (read-line in nil)))
			   puzzles)))
	(loop for puzzle in puzzles
		  do
			 (setf board (make-array '(9 9) :element-type 'smallish-integer
											:initial-contents puzzle
											:adjustable nil))
			 (solve)
			 (incf sum 
				   (+ (* 100 (aref board 0 0))
					  (* 10 (aref board 0 1))
					  (aref board 0 2))))
	sum))

(defun find-unassigned ()
  (declare (type (SIMPLE-ARRAY smallish-integer (9 9)) board))
  (loop named outer for row of-type smallish-integer from 0 below 9 do
	(loop for col of-type smallish-integer from 0 below 9
		  when (= (aref board row col) 0) do
			(return-from outer (values row col)))))

(defun box-match-p (tx ty digit)
  (declare (type smallish-integer tx ty digit))
  (declare (type (SIMPLE-ARRAY smallish-integer (9 9)) board))
  (loop named outer for i from tx below (+ tx 3) do
	(loop for j from ty below (+ ty 3)
		  when (= (aref board i j) digit) do
			(return-from outer t))))

(defun row-col-match-p (row col digit)
  (declare (type smallish-integer row col digit))
  (declare (type (SIMPLE-ARRAY smallish-integer (9 9)) board))
  (loop for i of-type smallish-integer from 0 below 9
		  thereis (or (= digit (aref board row i))
					  (= digit (aref board i col)))))

(defun conflict-p (row col digit)
  (declare (type smallish-integer row col digit))
  (or (row-col-match-p row col digit)
	  (box-match-p (- row (rem row 3))
				   (- col (rem col 3))
				   digit)))

(defun solve ()
  (declare (type (SIMPLE-ARRAY smallish-integer (9 9)) board))
  (multiple-value-bind (row col) (find-unassigned)
	(if (null row)
		t
		(loop for digit of-type smallish-integer from 1 upto 9 do
		  (when (not (conflict-p row col digit))
			(setf (aref board row col) digit)
			(if (solve)
				(return t)
				(setf (aref board row col) 0)))))))
