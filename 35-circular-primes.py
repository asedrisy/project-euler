from pyprimes import *

def rotations(n):
    s = len(str(n))
    p = 10 ** (s-1) if s > 2 else 10
    rots = []

    for x in xrange(s):
        n = ((n % p) * 10) + (n / p)
        rots.append(n)

    return rots

n = 0

for i in xrange(103, 1000000, 2):
    if all(isprime(x) for x in rotations(i)):
        n += 1
print n + 13
