from pyprimes import *

coeffs = []
for a in xrange(-1000, 1000):
    for b in xrange(-1000, 1000):
        coeffs.append((a,b))

max_n = 0
max_a, max_b = 0, 0

for v in coeffs:
    n = 0
    a = v[0]
    b = v[1]          
    while isprime(((n**2) + (a*n) + b)):
        n += 1
    if n > max_n:
        max_n = n
        max_a = a
        max_b = b

print max_a * max_b
    
