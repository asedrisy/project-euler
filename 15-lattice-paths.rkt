#lang racket

;; https://en.wikipedia.org/wiki/Central_binomial_coefficient

#|
(define (pascal n k)
  (cond
    ((or (zero? n) (= n k) (= k 0)) 1)
    (else (+ (pascal (- n 1) (- k 1))
             (pascal (- n 1) k)))))

(define (memoize function)
  (let ((table '()))
    (lambda args
      (apply values
             (cond ((assoc args table) => cdr)
                   (else
                    (call-with-values 
                     (lambda () (apply function args))
                     (lambda results
                       (set! table (cons (cons args results) table))
                       results))))))))
|#

(define (factorial n)
  (if (< n 1)
      1
      (* n (factorial (- n 1)))))

;; Binomial coefficent
(define (binomial-coefficient n k)
  (/ (factorial n)
     (* (factorial k) (factorial (- n k)))))

(binomial-coefficient 40 20)