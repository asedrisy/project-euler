class Switcher(object):
    
    def __init__(self):
        self.items = []
        self.index = 0
        self.s = 0
    
    def switch(self, n):
        for x in xrange(n):
            self.index += 1
            if self.index > len(self.items):
                self.index = 0

        self.s += 1 if self.index == 6 else 0

    
        
        
d = Switcher()
d.items = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat" "Sun"]
d.index = 1

for year in xrange(1901, 2001):
    # Jan 31
    d.switch(31)

    # Feb leap year
    if (year % 4 == 0) and (year % 100 != 0) or (year % 400 == 0):
        d.switch(29)
    else:
        d.switch(28)

    # March 31
    d.switch(31)

    # April 30
    d.switch(30)

    # May 31
    d.switch(31)
    #Jun
    d.switch(30)
    # Jul
    d.switch(31)
    # Aug
    d.switch(31)
    # Sept
    d.switch(30)
    # Okt
    d.switch(31)
    # Nov
    d.switch(30)
    # Dec
    d.switch(31)

print d.s
