(defun next-link (n)
  (declare (type fixnum n))
  (flet ((sq (x)
		   (declare (type fixnum x))
		   (* x x)))
	(loop while (> n 0)
		  sum (sq (mod n 10))
		  do (setf n (floor n 10)))))

(defun end-of-chain (n)
  (declare (type fixnum n))
  (let ((next-link (next-link n)))
	(if (or (= next-link 1) (= next-link 89))
		next-link
		(end-of-chain next-link)))) 

(defun test ()
  (loop for i from 1 upto 10000000
		count (= (end-of-chain i) 89)))
