from math import sqrt

solutions = {}
for i in range(1, 1001):
    solutions[i] = 0

for a in range(1000, 1, -1):
    for b in range(1000, 1, -1):
        p = a + b + sqrt(a*a + b*b)
        if p in solutions:
            solutions[p] += 1

print max(solutions, key=solutions.get)


