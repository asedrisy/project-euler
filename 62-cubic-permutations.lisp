(defun ordered-integers (n)
  (let ((rev 0))
	(loop for i from 9 downto 0 do
	  (loop for j = n then (floor j 10)
			while (> j 0)
			when (= (mod j 10) i) do
			  (setf rev (+ (* rev 10) i))
			end))
	rev))

(defun test (n)
  (let ((ht (make-hash-table :size 1000 :test 'eq)))
	(loop for i from 0 upto n
		  for cube = (expt i 3)
		  for ints = (ordered-integers cube)
		  unless (gethash ints ht) do
			(setf (gethash ints ht) 0)
		  end
		  do
			 (when (= ints 987655433210)
			   (print cube))
			 (incf (gethash ints ht))
			 (when (= (gethash ints ht) 5)
			   (return (list i cube ints))))))
	


