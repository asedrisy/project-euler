from itertools import *

pandigitals = []
pd_str = []

s = 0

def pd_property(p):
    if (int(p[1:4]) % 2 == 0) and (int(p[2:5]) % 3 == 0) and (int(p[3:6]) % 5 == 0) and (int(p[4:7]) % 7 == 0) \
       and (int(p[5:8]) % 11 == 0) and (int(p[6:9]) % 13 == 0) and (int(p[7:10]) % 17 == 0):
        return True
    return False

# Get all PD
for p in permutations("0123456789"):
    pandigitals.append(int("".join(p)))

# All unique
pandigitals = set(pandigitals)

# Turn into string
for p in pandigitals:
    pd_str.append(str(p))  

for p in pd_str:
    if pd_property(p):
        s += int(p)
print s



