from math import log, floor

def is_pandigital(k):
    return "123456789" == "".join(sorted(k))

values = []
for a in range(1, 500):
    for b in range(1, 5000):
        p = a * b
        s = str(a) + str(b) + str(p)
        if is_pandigital(s):
            print a, b, p
            values.append(int(p))
print sum(set(values))
