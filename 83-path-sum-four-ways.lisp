(load "p081_matrix.txt")

(defparameter ar (make-array '(80 80) :initial-contents input :adjustable nil))

(defparameter m2
  '((131 673 234 103 18)
	(201 96 342 965 150)
	(630 803 746 422 111)
	(537 699 497 121 956)
	(805 732 524 37 331)))

(defparameter ar2 (make-array '(5 5) :initial-contents m2 :adjustable nil))

(defun add-vertex (a graph cost-matrix x y top-p right-p down-p left-p)
  (unless (nth-value 1 (gethash a graph))
	(setf (gethash a graph) nil))
  (let ((top   (list x      (1- y)))
		(right (list (1+ x)  y))
		(down  (list x      (1+ y)))
		(left  (list (1- x) y)))
 	(when top-p
	  (push (cons top (aref cost-matrix (car top) (cadr top)))
			(gethash a graph)))
	(when right-p
	  (push (cons right (aref cost-matrix (car right) (cadr right)))
			(gethash a graph)))
	(when down-p
	  (push (cons down (aref cost-matrix (car down) (cadr down)))
			(gethash a graph)))
	(when left-p
	  (push (cons left (aref cost-matrix (car left) (cadr left)))
			(gethash a graph)))))

(defun make-graph (matrix)
  (let ((graph (make-hash-table :test #'equal)))
	(loop for x from 0 below (array-dimension matrix 0) do
	  (loop for y from 0 below (array-dimension matrix 1)
			for pos = (list x y)
			do
			   (let ((top-p (> y 0))
					 (right-p (< x (1- (array-dimension matrix 0))))
					 (down-p (< y (1- (array-dimension matrix 1))))
					 (left-p (> x 0)))
				 (add-vertex pos graph matrix x y top-p right-p down-p left-p))))
	graph))

(defun pprint-graph (graph)
  (loop for k being the hash-keys of graph using (hash-value v) do (format t "~a -> ~a~%" k v)))

(defparameter g (make-graph ar))
(defparameter g2 (make-graph ar2))

(defun minimum-distance-vertex (q distance)
  (let ((min most-positive-fixnum)
		(vert nil))
	(loop for v in q
		  when (< (gethash v distance) min)
			do
			   (setf min (gethash v distance))
			   (setf vert v))
	vert))


(defun dijkstra (graph &optional (source '(0 0)))
  (let ((distance (make-hash-table :test #'equal))
		(q '()))
	(loop for vertex being the hash-keys of graph
		  do
			 (unless (equal vertex source)
			   (setf (gethash vertex distance) most-positive-fixnum))
			 (push vertex q))
	(setf (gethash source distance) 0)
	(loop while (not (endp q))
		  for v = (minimum-distance-vertex q distance)
		  do
			 (setf q (remove v q :test #'equal))
			 (loop for u in (gethash v graph)
				   when (member (car u) q :test #'equal)
					 do
						(let ((alt (+ (gethash v distance)
									  (cdr u))))
						  (when (< alt (gethash (car u) distance))
							(setf (gethash (car u) distance) alt)))))
	distance))


(defun test ()
  (let ((g (dijkstra g2)))
	(gethash '(79 79) g2)))
