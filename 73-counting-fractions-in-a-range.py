from fractions import Fraction

test_value1 = 1.0 / 2.0
test_value2 = 1.0 / 3.0
f1 = Fraction(1, 3)
f2 = Fraction(1, 2)

values = []
N = 12000

for denom in range(1, N+1):
    found_right = False
    found_left = False
    denom_decim = denom * 1.0
    left_numer1 = 0
    right_numer1 = denom
    left_numer2 = 0
    right_numer2 = denom

    bound_left = 0
    bound_right = 1
    
    while not found_right:
        difference = right_numer1 - left_numer1
        mp_numer = (left_numer1 + right_numer1) / 2
        mp_value = mp_numer / denom_decim
                    
        if mp_value > test_value1:
            right_numer1 = mp_numer - 1
        elif mp_value < test_value1:
            left_numer1 = mp_numer
        elif mp_value == test_value1:
            bound_right = right_numer1 - 1
            found_right = True
        if difference == 0:
            bound_right = left_numer1
            found_right = True
            break
        elif difference == 1:
            right_value = right_numer1 / denom_decim
            left_value = left_numer1 / denom_decim
            if right_value > test_value1:
                bound_right = left_numer1
            elif right_value < test_value1:
                bound_right = right_numer1
            found_right = True

    while not found_left:
        difference = right_numer2 - left_numer2
        mp_numer = (left_numer2 + right_numer2) / 2
        mp_value = mp_numer / denom_decim

        if mp_value > test_value2:
            right_numer2 = mp_numer
        elif mp_value < test_value2:
            left_numer2 = mp_numer
        elif mp_value == test_value2:
            bound_left = right_numer2 + 1
            found_left = True
        if difference == 0:
            bound_left = left_numer2
            found_left = True
            break
        elif difference == 1:
            right_value = right_numer2 / denom_decim
            left_value = left_numer1 / denom_decim
            if left_value > test_value2:
                bound_left = left_numer2
            elif right_value > test_value2:
                bound_left = right_numer2
            found_left = True

    #print denom, bound_left, bound_right
    for n in range(bound_left, bound_right+1):
        f = Fraction(n, denom)
        if f1 < f < f2:
            values.append(f)

#print set(values)
print len(set(values))
#print values
#print sorted(set(values))
            
