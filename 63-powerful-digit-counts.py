def digit_count(n):
    s = 0
    while n > 0:
        s += 1
        n /= 10
    return s

def powers_below_b(b):
    a = 1
    n = 0
    while True:
        exp = a ** b
        d = digit_count(exp)
        if d == b:
            values[exp] = (a, d, exp)
            n += 1
        elif d > b:
            return n
        a += 1

values = {}

def try_powers(b):
    start = b/2
    low = start - 1
    high = start + 1
    low_inc = -1
    high_inc = 1
    n = 1 if digit_count(start ** b) == b else 0
    while True:
        if low_inc == 0 and high_inc == 0:
            return n
        if low_inc:
            dcount_low = digit_count(low ** b)
            n += 1 if dcount_low == b else 0
            if dcount_low < b:
                low_inc = 0
        if high_inc:
            dcount_high = digit_count(high ** b)
            n += 1 if dcount_high == b else 0
            print high
            if dcount_high > b:
                high_inc = 0
        low += low_inc
        high += high_inc

def main():
    
    n = 0
    for i in range(1, 50):
        n += powers_below_b(i)
        #n += try_powers(i)
    #print values
    print n

main()
