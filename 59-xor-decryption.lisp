(defparameter char-codes nil)
(defparameter test-string nil)

(defparameter possible-keys nil)

(defun read-codes ()
  (let ((int-strings (uiop:split-string (uiop:read-file-string "p059_cipher.txt") :separator ",")))
	(setf char-codes
		  (format nil "~{~a~}" (loop for ch in int-strings collect (code-char (parse-integer ch)))))
	(setf test-string (make-string (length char-codes)))
	(setf length-test-string (float (length test-string)))
	(setf possible-keys nil)
	t))

(defun test (pw-string)
  (let ((a (char-code (char pw-string 0)))
		(b (char-code (char pw-string 1)))
		(c (char-code (char pw-string 2))))
	(loop for i from 0 below (length char-codes) by 3
		  for x = i
		  for y = (+ i 1)
		  for z = (+ i 2)
		  do
			 (setf (char test-string x) (code-char (logxor (char-code (char char-codes x)) a))
				   (char test-string y) (code-char (logxor (char-code (char char-codes y)) b))
				   (char test-string z) (code-char (logxor (char-code (char char-codes z)) c))))
	(contains-english-words)))

(defun contains-english-words ()
  (and (search "and" test-string :test #'equalp)
	   (search "the" test-string :test #'equalp)))

(defun substrings (c n s)
  (if (>= (length s) n)
	  (when (test s)
		(push (list s (copy-seq test-string)) possible-keys))
	  (loop for x across c
			do
			   (substrings c n (concatenate 'string s (string x))))))

(defun sum-chars (pw)
  (let ((res (assoc pw possible-keys :test #'equalp)))
	(loop for c across (second res)
		  summing (char-code c))))

(defun run-tests ()
  (read-codes)
  (substrings "abcdefghijklmnopqrstuvwxyz" 3 "")
  (pprint possible-keys))


