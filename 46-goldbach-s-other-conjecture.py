from sympy import isprime, sieve
from array import array
from math import floor, sqrt

sieve._list = array('l', [2, 3, 5, 7, 11, 13])
squares = {}


def goldbach(n):
    for p in sieve.primerange(2, n):
        for sq in range(1, int(floor(sqrt(n))) + 1):
            if sq not in squares:
                squares[sq] = sq**2
            if n == (p + 2*squares[sq]):
                return True
    return False

n = 9
while True:
    if not (n in sieve):
        if not goldbach(n):
            print n
            break
    n += 2
