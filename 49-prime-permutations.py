from sympy import sieve, isprime
from itertools import permutations

for n in range(999, 9999):
    if isprime(n):
        perms = []
        difference = {}
        s = str(n)
        for p in permutations(s):
            perms.append(int("".join(p)))
        primes = [p for p in perms if isprime(p)]
        
        for i in primes:
            for j in primes:
                d = abs(i - j)
                if d in difference:
                    difference[d].append(i)
                else:
                    difference[d] = [i]
        for k, v in difference.iteritems():
            d = 0
            w = sorted(set(v))
            if len(w) == 3:
                if abs(w[0] - w[1]) == abs(w[1] - w[2]):
                    print w            
            
