from math import sqrt

b = 1
for b in range(1000):
    for a in range(1, b):
        
        a2 = a**2
        b2 = b**2
        c = sqrt(a2 + b2)
        if c % 1 == 0 and (a + b + c) == 1000:
            print a, b, c, '->', a*b*c
            break
    b += 1
print b
